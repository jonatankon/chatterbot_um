# Chatbot de información

Este repositorio contiene el proyecto realizado por los alumnos de la cátedra de Inteligencia Artificial de la Facultad de Ingeniería de la Universidad de Mendoza, Sede San Rafael.

## Objetivo 

Realizar un chatbot que permita a los usuarios realizar consultas y preguntas sobre diferentes aspectos del funcionamiento de la Universidad y las carreras que ofrece.

## Librerías utilizadas

	- Chatterbot

## Integrantes
	
	Alumnos :
		- Julian Ariel Campana 
		- Ornella Mónica Grasso Viola	
		-@Jonatan Matias Kondratiuk
		- Fernando Simón López
		- Matias Nicolas Romani
		- Marcos Rugoso
		
	Profesora : Ing. Andrea Navarro Moreno
